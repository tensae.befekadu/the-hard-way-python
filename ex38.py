ten_things = "Laptop Mobile Refrigerator TV Telephone Dark"

stuff = ten_things.split(' ')
more_stuff = ["Day", "Night", "Worship", "GOD",
              "Road", "Right", "Left"]

while len(stuff) != 10:
    next_one = more_stuff.pop()
    print("Adding: ", next_one)
    stuff.append(next_one)
    print(f"{len(stuff)} items now")

print(stuff)
print(stuff[1])
print(stuff[-1])
print(stuff.pop())
print(' '.join(stuff))
print('#'.join(stuff[3:5]))

print(more_stuff)
