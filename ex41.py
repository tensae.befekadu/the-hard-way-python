class Dog:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def bark(self):
        print("Woof Woof")

    def doginfo(self):
        print(f"{self.name} is {self.age} years old")

    def setbuddy(self, buddy):
        self.buddy = buddy
        buddy.buddy = self


chilo = Dog("Chilo", 8)
mechal = Dog("Mechal", 11)
pal = Dog("Pal", 3)
pal.setbuddy(chilo)

print(pal.doginfo())
print(mechal.doginfo())
print(pal.doginfo())
print(pal.buddy.name)
