# Animal is-a object
class Animal(object):
    pass


# Dog has-a object
class Dog(Animal):
    def __init__(self, name):
        self.name = name


# Cat has-a object relationship
class Cat(Animal):
    def __init__(self, name):
        self.name = name


# Person is-a object
class Person(object):
    def __init__(self, name):
        self.name = name
        self.pet = None


# has-a
class Employee(Person):
    def __init__(self, name, salary):
    super(Employee, self).__init__(name)
    self.salary = salary


rover = Dog("Rover")
satan = Cat("Satan")
mary = Person("Mary")
mary.pet = satan

frank = Employee("Frank", 12000)
frank.pet = rover
