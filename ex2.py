# A comment, this is so you can read your program later
# Anything after # is ignored by python

print("I could have code like this.")  # and the comment after is ignored

# you can also use a comment to "Disable" or comment out code:
# print("this wont run.")

print("This will run.")
