def add(num1, num2):
    total = num1 + num2
    print(f"The sum of {num1} and {num2} is {total}")
    return total


def sub(num1, num2):
    total = num1-num2
    print(f"The subtraction of {num1} and {num2} is {total}")
    return total


def mult(num1, num2):
    total = num1*num2
    print(f"The multiple of {num1} and {num2} is {total}")
    return total


def div(num1, num2):
    total = num1/num2
    print(f"The division of {num1} and {num2} is {total}")
    return total


num1 = int(input("Enter the first number: "))
num2 = int(input("Enter the second Number: "))

add(num1, num2)
sub(num1, num2)
mult(num2, num2)
div(num1, num2)
