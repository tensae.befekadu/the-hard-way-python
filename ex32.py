countz = [1, 2, 3, 4, 5]

for number in countz:
    print(f"This is count {number}")

element = []

for i in range(0, 6):
    print(f"adding {i} to the list")
    element.append(i)

print(element)
