try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'the_hard_way_python',
    'author': 'Tensae',
    'url' : 'https://gitlab.com/tensae.befekadu/the-hard-way-python',
    'author_email' : 'tensaeb2017@gmail.com',
    'version' : '0.1',
    'instLl_requires' : ['nose'],
    'packages' : [],
    'scripts' : [],
    'name' : 'Python the hard way'
}

setup(**config)