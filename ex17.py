from sys import argv
from os.path import exists

scripts, from_file, to_file = argv

print(f"Compying from {from_file} to {to_file}")

# Could do these two on one lines, how?
indata = open(from_file).read()
# indata = in_file.read()


print(f"The input file is {len(indata)} bytes long")

print(f"Does the output file exis? {exists(to_file)}")
print("Ready, hit Return to continue, ctrl-c to abort.")
input()

out_file = open(to_file, 'w')
out_file.write(indata)

print("Alright, all done.")

out_file.close()
